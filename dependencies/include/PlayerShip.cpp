#include "stdafx.h"
#include "PlayerShip.h"
#include "aimAtMouse.h"

PlayerShip::PlayerShip(sf::Sprite pSprite, int pScreenWidth)
{
	mSprite = pSprite;
	mScreenWidth = pScreenWidth;
	life = 100;
	mVisible = true;
	position.x = 2035;
	position.y = 2250;
	mSprite.setOrigin(sf::Vector2f((mSprite.getLocalBounds().width / 2) + 2, mSprite.getLocalBounds().height / 2));
}

PlayerShip::~PlayerShip()
{
}

void PlayerShip::Update(float deltatime)
{

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		mSprite.rotate(-2.0f);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		mSprite.rotate(2.0f);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		float angleRADS = (mSprite.getRotation()) / 180 * M_PI;
		float newPosY = 1.0f*sin(angleRADS - M_PI / 2);
		float newPosX = 1.0f*cos(angleRADS - M_PI / 2);
		direction.x = newPosX;
		direction.y = newPosY;
		float angle = mouseAim->to_angle(mouseAim->normalized(direction));
		
		velocity.x += (accel*direction.x);
		velocity.y += (accel*direction.y);

		Move(direction.x, direction.y);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		velocity.x *= decel;
		velocity.y *= decel;
	}

	velocity.x *= decel;
	velocity.y *= decel;

	if (velocity.x < -maxspeed) velocity.x = -maxspeed;
	if (velocity.x > maxspeed) velocity.x = maxspeed;
	if (velocity.y < -maxspeed) velocity.y = -maxspeed;
	if (velocity.y > maxspeed) velocity.y = maxspeed;

	position += velocity;
	mSprite.setPosition(position);
	Life(1);
}

void PlayerShip::Move(float x, float y)
{
	position.x = position.x + x;
	position.y = position.y + y;
}

sf::Sprite PlayerShip::GetSprite()
{
	return mSprite;
}

float PlayerShip::GetX()
{
	return position.x;
	
}

float PlayerShip::GetY()
{
	return position.y;
}

int PlayerShip::Life(int pLife)
{
	life -= pLife;
	if (life < 50)
	{
		NearDeath();
		return 2;
	}
	if (life <= 0)
	{
		Death();
		return 0;

	}
	return 1;
}

bool PlayerShip::IsVisible()
{
	return true;
}

void PlayerShip::NearDeath()
{
	//Change sprite
}

void PlayerShip::Death()
{
	//Change sprite
	life = 0;
	std::cout << "Dead" << std::endl;
}

Vector2f PlayerShip::GetVPos()
{
	return position;
}

void PlayerShip::Draw(sf::RenderWindow & App)
{
	App.draw(mSprite);
}
