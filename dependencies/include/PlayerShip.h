#pragma once
class aimAtMouse;

class PlayerShip : public sf::Transformable
{
public:
	PlayerShip(sf::Sprite pSprite, int pScreenWidth);
	~PlayerShip();
	void Update(float deltatime);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	int Life(int);
	bool IsVisible();
	void NearDeath();
	void Death();
	void Move(float x, float y);
	Vector2f GetVPos();
	void Draw(sf::RenderWindow& App);
private:
	PlayerShip() {};
	sf::Sprite mSprite;

	float mX;
	float mY;
	int mScreenWidth;
	bool mVisible;
	int life;
	sf::Event Event;
	sf::Vector2f position;
	sf::Vector2f velocity;
	sf::Vector2f direction;
	aimAtMouse* mouseAim;
	float maxspeed = 3.0f;
	float accel = 0.05f;
	float decel = 0.975f;
	
};