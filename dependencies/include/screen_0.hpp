#pragma once
#include <iostream>
#include "stdafx.h"
#include "cScreen.hpp"
#include "SoundManager.h"


class screen_0 : public cScreen
{
private:
	sf::View view1;
	int alpha_max;
	int alpha_div;
	bool playing;
	sf::Texture backgroundImage;
	SoundManager soundmanager;
	sf::Sound sound;
public:
	screen_0(void);
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime);
};

screen_0::screen_0(void)
{
	alpha_max = 3 * 255;
	alpha_div = 3;
	playing = false;
	assert(backgroundImage.loadFromFile("../data/Title.PNG"));
}

int screen_0::Run(sf::RenderWindow &App, sf::Clock deltaTime)
{
	view1.reset(sf::FloatRect(0, 0, 800, 501));
	sf::Event Event;
	bool Running = true;
	sf::Texture Texture;
	sf::Sprite Sprite;
	int alpha = 0;
	sf::Font Font;
	sf::Text Menu1;
	sf::Text Menu2;
	sf::Text Menu3;
	int menu = 0;
	
	std::string soundlocation = "../data/Robot_blip.wav";
	sound = soundmanager.loadSound(soundlocation);
	

	sf::Sprite backgroundSprite(backgroundImage);
	backgroundSprite.setTextureRect(sf::IntRect(0, 0, 800, 600));

	Sprite.setColor(sf::Color(255, 255, 255, alpha));
	if (!Font.loadFromFile("../data/AGENCYR.TTF"))
	{
		std::cerr << "Error loading AGENCYR.TTF" << std::endl;
		return (-1);
	}
	Menu1.setFont(Font);
	Menu1.setCharacterSize(20);
	Menu1.setString("Play");
	Menu1.setPosition({ 100.f - Menu1.getCharacterSize(), 220.f });
	
	Menu2.setFont(Font);
	Menu2.setCharacterSize(20);
	Menu2.setString("Exit");
	Menu2.setPosition({ 100.f - Menu2.getCharacterSize(), 280.f });

	Menu3.setFont(Font);
	Menu3.setCharacterSize(20);
	Menu3.setString("Continue");
	Menu3.setPosition({ 100.f - Menu3.getCharacterSize(), 220.f });

	if (playing)
	{
		alpha = alpha_max;
	}

	while (Running)
	{
		while (App.pollEvent(Event))
		{
			if (Event.type == sf::Event::Closed)
			{
				return (-1);
			}

			if (Event.type == sf::Event::KeyPressed)
			{
				switch (Event.key.code)
				{
				case sf::Keyboard::Up:
					menu = 0;
					break;
				case sf::Keyboard::Down:
					menu = 1;
					break;
				case sf::Keyboard::Return:
					if (menu == 0)
					{
						playing = true;
						sound.play();
						return(1);
					}
					else
					{
						sound.play();
						return (-1);
					}
					break;
				default:
					break;
				}
			}
		}
		if (alpha < alpha_max)
		{
			alpha++;
		}
		Sprite.setColor(sf::Color(255, 255, 255, alpha / alpha_div));
		if (menu == 0)
		{
			Menu1.setColor(sf::Color(255, 0, 0, 255));
			Menu2.setColor(sf::Color(255, 255, 255, 255));
			Menu3.setColor(sf::Color(255, 0, 0, 255));
		}
		else
		{
			Menu1.setColor(sf::Color(255, 255, 255, 255));
			Menu2.setColor(sf::Color(255, 0, 0, 255));
			Menu3.setColor(sf::Color(255, 255, 255, 255));
		}

		App.clear();
		App.setView(view1);
		App.draw(Sprite);
		App.draw(backgroundSprite);
		if (alpha == alpha_max)
		{
			if (playing)
			{
				App.draw(Menu3);
			}
			else
				App.draw(Menu1);
			App.draw(Menu2);
		}
		App.display();
	}

	return (-1);
}