#pragma once


class SoundManager
{
public:
	SoundManager();
	~SoundManager();
	sf::Sound loadSound(std::string &sound);
	sf::Music loadMusic(std::string &music);
private:
	sf::Sound noice;
	sf::SoundBuffer buffer;
};