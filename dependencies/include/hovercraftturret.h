#pragma once
class aimAtMouse;

class PlayerProjectile;
class hovercraftturret
{
public:
	hovercraftturret(sf::Sprite pSprite, sf::Sprite projSprite, Vector2f spritePos, int viewsizeX, int viewsizeY);
	~hovercraftturret();
	void Update(float deltatime,Vector2i mousePos, Vector2f spritePos);
	void RotateTurret(Vector2f spritePos, Vector2i mousePos);
	sf::Sprite GetSprite();
	void Draw(sf::RenderWindow& App);
	float GetX();
	float GetY();
	bool IsVisible();
private:
	hovercraftturret() {};
	sf::Sprite mSprite;
	sf::Sprite mProjSprite;
	int mScreenWidth;
	bool mVisible;
	sf::Event Event;
	sf::Vector2f position;
	aimAtMouse* mouseAim;
	float sidex;
	float sidey;
	float degrees;
	int width;
	int height;
	bool lastPressed;
	std::vector<PlayerProjectile*> playerprojV;
};
