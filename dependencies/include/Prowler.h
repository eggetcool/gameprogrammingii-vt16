#pragma once

class PlayerShip;

class Prowler : public sf::Transformable
{
public:
	Prowler(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f SpritePos, PlayerShip* pShip);
	~Prowler();
	void Update(float deltatime, sf::Vector2f SpritePos);
	sf::Sprite GetSprite();
	
	float GetX();
	float GetY();
	int Life(int);
	bool IsVisible();
	void Death();
	void Move(float x, float y);
	Vector2f GetVPos();
	void Draw(sf::RenderWindow& App);
	void normalize(sf::Vector2f& rhs);

private:
	Prowler() {};
	sf::Sprite mSprite;

	PlayerShip* pShip;

	float mX;
	float mY;
	int mScreenWidth;
	bool mVisible;
	int life;
	float movementSpeed;

	sf::Vector2f shipPosition;
	sf::Vector2f position;
	sf::Vector2f velocity;

	sf::RectangleShape aggroBox;

	float maxspeed = 3.0f;
};

