#include "stdafx.h"
#include "cScreen.hpp"
#include "SoundManager.h"
#include "PlayerShip.h"
#include "hovercraftturret.h"
#include "Prowler.h"
#include "PlayerProjectile.h"
#include "Shield.h"

class screen_1 : public cScreen
{
public:
	screen_1(void);
	~screen_1();
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime);
	void Update(float deltatime);
	void Draw(sf::RenderWindow & App, sf::Sprite backgroundSprite);
private:
	sf::View view1;
	sf::RectangleShape Rectangle;
	sf::Texture ship;
	sf::Texture t_prowler;
	sf::Texture t_shield;
	sf::Texture turret;
	sf::Texture backgroundImage;
	sf::Texture penumbraTexture;
	sf::Texture projectile;
	sf::Shader unshadowShader;
	sf::Shader lightOverShapeShader;
	sf::IntRect r1;
	PlayerShip* o_PlayerShip;
	Prowler* o_Prowler;
	Shield* o_Shield;
	PlayerProjectile* o_Projectile;
	hovercraftturret* o_Hovercraftturret;
	sf::Vector2i globalMousePosition;
	int viewsizeX = 600;
	int viewsizeY = 400;
};

screen_1::screen_1(void)
{
	r1.top = 20;
	r1.left = 0;
	r1.height = 57;
	r1.width = 27;
	assert(ship.loadFromFile("../data/hovercraft_idle_vertical.png", r1));
	assert(turret.loadFromFile("../data/hovercraft_turret.png"));
	assert(backgroundImage.loadFromFile("../data/Level_1_v2.png"));
	assert(unshadowShader.loadFromFile("../data/unshadowShader.vert", "../data/unshadowShader.frag"));
	assert(lightOverShapeShader.loadFromFile("../data/lightOverShapeShader.vert", "../data/lightOverShapeShader.frag"));
	assert(penumbraTexture.loadFromFile("../data/penumbraTexture.png"));
	assert(t_prowler.loadFromFile("../data/prowler_texture.png"));
	assert(projectile.loadFromFile("../data/bullet_proj.png"));
	assert(t_shield.loadFromFile("../data/shield.png"));
	penumbraTexture.setSmooth(true);
}
screen_1::~screen_1()
{
	delete o_Hovercraftturret;
	delete o_Prowler;
	delete o_PlayerShip;
	delete o_Shield;
	
}

int screen_1::Run(sf::RenderWindow &App, sf::Clock deltaTime)
{
	sf::Event Event;
	bool Running = true;

	sf::Sprite backgroundSprite(backgroundImage);
	sf::Sprite playerShipSprite(ship);
	sf::Sprite hovercraftsprite(turret);
	sf::Sprite prowlerSprite(t_prowler);
	sf::Sprite playerProjSprite(projectile);
	sf::Sprite shieldSprite(t_shield);

	o_PlayerShip = new PlayerShip(playerShipSprite, App.getSize().x);
<<<<<<< HEAD
	o_Prowler = new Prowler(prowlerSprite, App.getSize().x, o_PlayerShip->GetVPos());
	o_Shield = new Shield(shieldSprite, App.getSize().x, o_PlayerShip->GetVPos());
=======
	o_Prowler = new Prowler(prowlerSprite, App.getSize().x, o_PlayerShip->GetVPos(), o_PlayerShip);
>>>>>>> 2c62872d7a99e86cc661370619e922b8f29d5758
	o_Hovercraftturret = new hovercraftturret(hovercraftsprite, playerProjSprite, o_PlayerShip->GetVPos(), App.getSize().x, App.getSize().y);
	backgroundSprite.setTextureRect(sf::IntRect(0, 0, 4961, 3508));

	/*ltbl::LightSystem ls;
	ls.create(sf::FloatRect(-1000.0f, -1000.0f, 1000.0f, 1000.0f), App.getSize(), penumbraTexture, unshadowShader, lightOverShapeShader);
	std::shared_ptr<ltbl::LightPointEmission> light = std::make_shared<ltbl::LightPointEmission>();*/

	//sf::Texture pointLightTexture;
	//pointLightTexture.loadFromFile("../data/pointLightTexture.png");
	//pointLightTexture.setSmooth(true);

	//light->_emissionSprite.setOrigin(sf::Vector2f(0.f, 0.f));
	//light->_emissionSprite.setTexture(pointLightTexture);
	//light->_emissionSprite.setColor(sf::Color::Blue);
	//light->_emissionSprite.setPosition(sf::Vector2f(10, 10));
	//light->_localCastCenter = sf::Vector2f(0.0f, 0.0f);  // This is where the shadows emanate from relative to the sprite

	//ls.addLight(light);
	//ls.render(view1, unshadowShader, lightOverShapeShader);

	view1.reset(sf::FloatRect(o_PlayerShip->GetX(), o_PlayerShip->GetY(), viewsizeX, viewsizeY));

	while (Running)
	{
		if (o_PlayerShip->Life(0) == 0)
		{
			return 2;
		}
		sf::Time lasttime = deltaTime.restart();
		float millideltatime = lasttime.asMilliseconds() / 1000.0f;
		std::cout << millideltatime << std::endl;
		//Verifying events
		while (App.pollEvent(Event))
		{
			// Window closed
			if (Event.type == sf::Event::Closed)
			{
				return (-1);
			}
			//Key pressed
			if (Event.type == sf::Event::KeyPressed)
			{
				switch (Event.key.code)
				{
				case sf::Keyboard::Escape:
					return (0);
					break;
				default:
					break;
				}
			}
			if (Event.type == sf::Event::MouseMoved)
			{
				globalMousePosition = sf::Mouse::getPosition(App);
			}
		}
		view1.reset(sf::FloatRect(o_PlayerShip->GetX() - viewsizeX / 2, o_PlayerShip->GetY() - viewsizeY / 2, viewsizeX, viewsizeY));
		Update(millideltatime);
		Draw(App, backgroundSprite);
	}
	
	//Never reaching this point normally, but just in case, exit the application
	return -1;
}

void screen_1::Update(float deltaTime)
{
	o_PlayerShip->Update(deltaTime);
	o_Hovercraftturret->Update(deltaTime, globalMousePosition, o_PlayerShip->GetVPos());
	o_Hovercraftturret->RotateTurret(o_PlayerShip->GetVPos(), globalMousePosition);
	o_Prowler->Update(deltaTime, o_PlayerShip->GetVPos());
	o_Shield->Update(deltaTime, o_PlayerShip->GetVPos());
}

void screen_1::Draw(sf::RenderWindow& App, sf::Sprite backgroundSprite)
{
	App.clear();
	App.setView(view1);
	App.draw(backgroundSprite);
	o_PlayerShip->Draw(App);
	o_Hovercraftturret->Draw(App);
	o_Prowler->Draw(App);
	o_Shield->Draw(App);
	App.display();
	
}