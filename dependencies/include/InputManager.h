#pragma once


enum InputAxis
{
	InputAxis_Horizontal,
	InputAxis_Vertical,
	InputAxis_MouseX,
	InputAxis_MouseY
};

class InputManager
{

public:

	InputManager(sf::Window* p_pWindow);
	~InputManager();

	bool Update(sf::Time p_DeltaTime);

	bool GetKey(sf::Keyboard::Key p_KeyCode);
	bool GetKeyDown(sf::Keyboard::Key p_KeyCode);
	bool GetKeyUp(sf::Keyboard::Key p_KeyCode);

	bool GetMouseButton(int p_Index);
	bool GetMouseButtonDown(int p_Index);
	bool GetMouseButtonUp(int p_Index);

	float GetAxis(InputAxis p_InputAxis);
	int GetAxisRaw(InputAxis p_InputAxis);

	sf::Vector2i GetMousePosition();

private:

	void OnKeyChange(sf::Keyboard::Key p_KeyCode, bool p_Value);
	void OnMouseButtonChange(int p_Index, bool p_Value);
//	void OnFocusChange(bool p_Value);

	void HorizontalInput(float p_DeltaTime);
	void VerticalInput(float p_DeltaTime);

	struct AxisInput
	{
		float m_AxisGravity;

		float m_Horizontal;
		sf::Keyboard::Key m_Horizontal_Neg, m_Horizontal_Pos;
		float m_Vertical;
		sf::Keyboard::Key m_Vertical_Neg, m_Vertical_Pos;
		float m_MouseX, m_MouseY;
	};

	AxisInput m_AxisInput;
	sf::Vector2i m_MousePos;
	sf::Window* m_pWindow;

	bool m_MouseButtonState[3];
	bool m_MouseButtonDown[2][3];
	bool m_MouseButtonUp[2][3];

	bool m_KeyState[101];
	bool m_KeyDownState[2][101];
	bool m_KeyUpState[2][101];

};