#pragma once
#include "aimAtMouse.h"

class PlayerProjectile
{
public:
	PlayerProjectile(sf::Sprite pSprite, Vector2f position, Vector2f direction, int viewsizeX, int viewsizeY);
	~PlayerProjectile();
	void Update(float deltatime, Vector2i mousePos);
	sf::Sprite GetSprite();
	bool IsVisible();
	bool IsActive();
	void Deactivate();
	void Activate();
	void Move(float x, float y);
	Vector2f GetVPos();
private:
	PlayerProjectile() {};
	sf::Sprite mSprite;


	bool mVisible;
	bool mActive;
	sf::Event Event;
	sf::Vector2f position;
	sf::Vector2f direction;
	aimAtMouse* mouseAim;
	
	float maxspeed;
	float currentSpeed;
	float accel;
	int width;
	int height;
};
