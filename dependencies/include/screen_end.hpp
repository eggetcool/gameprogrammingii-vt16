#include "stdafx.h"
#include "cScreen.hpp"


class screen_end : public cScreen
{
private:
	sf::VideoMode vidMode;
	sf::Texture backgroundImage;
public:
	screen_end(void);
	virtual int Run(sf::RenderWindow &App, sf::Clock deltaTime);
};

screen_end::screen_end(void)
{
	assert(backgroundImage.loadFromFile("../data/Title.PNG"));
	backgroundImage.setRepeated(true);
}


int screen_end::Run(sf::RenderWindow &App, sf::Clock deltaTime)
{
	sf::Font Font;
	sf::Text Losetext;
	sf::Text Returntext;
	sf::Event Event;
	bool Running = true;
	sf::Sprite backgroundSprite(backgroundImage);
	backgroundSprite.setTextureRect(sf::IntRect(0, 0, vidMode.width, vidMode.height));
	
	if (!Font.loadFromFile("../data/AGENCYR.TTF"))
	{
		std::cerr << "Error loading AGENCYR.TTF" << std::endl;
		return (-1);
	}
	Losetext.setFont(Font);
	Losetext.setCharacterSize(20);
	Losetext.setString("Game Over");
	Losetext.setPosition({ 400.f - Losetext.getCharacterSize(), 220.f });

	Returntext.setFont(Font);
	Returntext.setCharacterSize(20);
	Returntext.setString("Press <return> to start over, <escape> to exit");
	Returntext.setPosition({ 400.f - Returntext.getCharacterSize(), 280.f });

	while (Running)
	{
		//Verifying events
		while (App.pollEvent(Event))
		{
			// Window closed
			if (Event.type == sf::Event::Closed)
			{
				return (-1);
			}
			//Key pressed
			if (Event.type == sf::Event::KeyPressed)
			{
				switch (Event.key.code)
				{
				case sf::Keyboard::Escape:
					return (-1);
					break;
				case sf::Keyboard::Return:			//kanske fel.
					return (1);
					break;
				default:
					break;
				}
			}
		}
		//Clearing screen
		App.clear();
		App.draw(backgroundSprite);
		App.display();
	}

	//Never reaching this point normally, but just in case, exit the application
	return -1;
}