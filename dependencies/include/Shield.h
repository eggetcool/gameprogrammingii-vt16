#pragma once

class Shield : public sf::Transformable
{
public:
	Shield(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f SpritePos);
	~Shield();
	void Update(float deltatime, sf::Vector2f SpritePos);
	sf::Sprite GetSprite();

	float GetX();
	float GetY();
	
	bool IsVisible();
	void Disappear();

	Vector2f GetVPos();
	void Draw(sf::RenderWindow& App);

private:
	Shield() {};
	sf::Sprite mSprite;

	float mX;
	float mY;
	int mScreenWidth;
	bool mVisible;

	sf::Vector2f shipPosition;
	sf::Vector2f position;
};

