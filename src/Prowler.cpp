#include "stdafx.h"
#include "Prowler.h"
#include "PlayerShip.h"

Prowler::Prowler(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f SpritePos, PlayerShip* pShip)
{
	mSprite = pSprite;
	mScreenWidth = pScreenWidth;
	life = 100;
	mVisible = true;

	position.x = 2035;
	position.y = 2075;
	mSprite.setPosition(position);

	this->pShip = pShip;

	shipPosition = SpritePos;
	movementSpeed = 2;

	mSprite.setOrigin(sf::Vector2f(mSprite.getLocalBounds().width / 2, mSprite.getLocalBounds().height / 2));
}

Prowler::~Prowler()
{

}

void Prowler::Update(float deltatime, sf::Vector2f SpritePos)
{
	shipPosition = SpritePos;

	sf::Vector2f shipDirection = shipPosition - mSprite.getPosition();
	float length = sqrt(pow(shipDirection.x, 2) + pow(shipDirection.y, 2));

	if (length < 150)
	{
		normalize(shipDirection);
		position += shipDirection * movementSpeed;
		mSprite.setPosition(position);

		if (length < 50)
		{
			if (pShip != nullptr)
			{
				pShip->Life(-100);
			}
		}
	}

		sf::Vector2f centerDirection = sf::Vector2f(2035, 2075) - mSprite.getPosition();
		length = sqrt(pow(centerDirection.x, 2) + pow(centerDirection.y, 2));

		if (length > 5)
		{
			normalize(centerDirection);
			position += centerDirection * movementSpeed;
			mSprite.setPosition(position);
		}
}

sf::Sprite Prowler::GetSprite()
{
	return mSprite;
}

float Prowler::GetX()
{
	return position.x;
}

float Prowler::GetY()
{
	return position.y;
}

int Prowler::Life(int pLife)
{
	life += pLife;
	if (life <= 0)
	{
		Death();
		return 0;
	}
	return 1;
}

bool Prowler::IsVisible()
{
	return false;
}

void Prowler::Death()
{
}

void Prowler::Move(float x, float y)
{
}

Vector2f Prowler::GetVPos()
{
	return position;
}

void Prowler::Draw(sf::RenderWindow & App)
{
	App.draw(mSprite);
}

void Prowler::normalize(Vector2f& rhs)
{
	float len = sqrt(pow(rhs.x, 2) + pow(rhs.y, 2));
	if (len > 0.0f)
	{
		rhs.x /= len;
		rhs.y /= len;
	}
}
