#include "stdafx.h"
#include "PlayerProjectile.h"
#include "Sprite.h"
#include "aimAtMouse.h"



PlayerProjectile::PlayerProjectile(sf::Sprite pSprite, Vector2f position, Vector2f direction, int windowsizeX, int windowsizeY)
{
	mVisible = true;
	mActive = false;
	maxspeed = 20.0f;
	currentSpeed = 1.f;
	accel = 0.05f;
	width = windowsizeX;
	height = windowsizeY;
	this->direction = direction;
	this->position = position;
	mSprite = pSprite;
	mSprite.setPosition(position);
	mSprite.setOrigin(((mSprite.getLocalBounds().width / 2) - 0.5), (mSprite.getLocalBounds().height / 2));
	mSprite.setScale(0.68, 0.4);

}

PlayerProjectile::~PlayerProjectile()
{

}

void PlayerProjectile::Update(float deltatime, Vector2i mousePos)
{
	position += direction * maxspeed;
	mSprite.setPosition(position);
	if (position.x > 4961)
	{
		mActive = false;
	}
	else if (position.y > 3508)
	{

	}
}

sf::Sprite PlayerProjectile::GetSprite()
{
	return mSprite;
}

bool PlayerProjectile::IsVisible()
{
	return mVisible;
}

bool PlayerProjectile::IsActive()
{
	return mActive;
}

void PlayerProjectile::Deactivate()
{
	mActive = false;
}

void PlayerProjectile::Activate()
{
	mActive = true;
}

void PlayerProjectile::Move(float x, float y)
{
	position.x = position.x + x;
	position.y = position.y + y;
}

Vector2f PlayerProjectile::GetVPos()
{
	return position;
}
