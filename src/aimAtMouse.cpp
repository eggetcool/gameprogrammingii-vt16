#include "stdafx.h"
#include <Vector>
#include "aimAtMouse.h"
#define PI 3.14159265f


aimAtMouse::aimAtMouse()
{
}

// math functions used
float aimAtMouse::length(const Vector2f& rhs)
{
	return sqrtf(rhs.x * rhs.x + rhs.y * rhs.y);
}

void aimAtMouse::normalize(Vector2f& rhs)
{
	float len = length(rhs);
	if (len > 0.0f)
	{
		rhs.x /= len;
		rhs.y /= len;
	}
}

Vector2f aimAtMouse::normalized(const Vector2f& rhs)
{
	Vector2f result(rhs);
	normalize(result);
	return result;
}

float aimAtMouse::to_angle(const Vector2f& rhs)
{
	return atan2f(rhs.y, rhs.x) * (180.0f / PI);
}
