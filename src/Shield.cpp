#include "stdafx.h"
#include "Shield.h"
#include "PlayerShip.h"

Shield::Shield(sf::Sprite pSprite, int pScreenWidth, sf::Vector2f SpritePos)
{
	mSprite = pSprite;
	mScreenWidth = pScreenWidth;
	mVisible = true;
	position.x = 2100;
	position.y = 1900;
	shipPosition = SpritePos;
	
	mSprite.setOrigin(sf::Vector2f(mSprite.getLocalBounds().width / 2, mSprite.getLocalBounds().height / 2));
}

Shield::~Shield()
{
}

void Shield::Update(float deltatime, sf::Vector2f SpritePos)
{
	shipPosition = SpritePos;

	sf::RectangleShape hitBox(sf::Vector2f(50, 50));
	hitBox.setFillColor(sf::Color(100, 250, 50));
	hitBox.setPosition(mSprite.getOrigin().x, mSprite.getOrigin().y);

	mSprite.setPosition(position);
}

sf::Sprite Shield::GetSprite()
{
	return mSprite;
}

float Shield::GetX()
{
	return position.x;
}

float Shield::GetY()
{
	return position.y;
}

bool Shield::IsVisible()
{
	return true;
}

void Shield::Disappear()
{
}

Vector2f Shield::GetVPos()
{
	return position;
}

void Shield::Draw(sf::RenderWindow & App)
{
	App.draw(mSprite);

}