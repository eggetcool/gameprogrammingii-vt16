#include "stdafx.h"
#include "hovercraftturret.h"
#include "MathExtension.h"
#include "aimAtMouse.h"
#include "PlayerProjectile.h"
#include <math.h>
#define PI 3.14159265f

hovercraftturret::hovercraftturret(sf::Sprite pSprite, sf::Sprite projSprite, Vector2f spritePos, int viewsizeX, int viewsizeY)
{
	mSprite = pSprite;
	mProjSprite = projSprite;
	mVisible = true;
	lastPressed = false;
	position.x = spritePos.x;
	position.y = spritePos.y;
	width = viewsizeX;
	height = viewsizeY;
	mSprite.setPosition(position);
}

hovercraftturret::~hovercraftturret()
{
	auto it = playerprojV.begin();
	while (it != playerprojV.end())
	{
		delete (*it);
		++it;
	}
}

void hovercraftturret::Update(float deltatime, Vector2i mousePos, Vector2f spritePos)
{
	mSprite.setOrigin(sf::Vector2f((mSprite.getLocalBounds().width / 2) - 3, (mSprite.getLocalBounds().height / 2) - 0.5));
	position.x = spritePos.x;
	position.y = spritePos.y;
	mSprite.setPosition(position);

	bool pressed = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	if (pressed && !lastPressed)
	{
		Vector2f diff;
		diff.x = mousePos.x - (width / 2);
		diff.y = mousePos.y - (height / 2);
		mouseAim->normalize(diff);
		float angle = mouseAim->to_angle(diff);

		mProjSprite.setRotation(angle + 90);
		PlayerProjectile* o_Projectile = new PlayerProjectile(mProjSprite, position, diff, width, height);
		playerprojV.push_back(o_Projectile);
	}

	for (int x = 0; x < playerprojV.size(); x++)
	{
		playerprojV[x]->Update(deltatime, mousePos);
	}
	lastPressed = pressed;
}

void hovercraftturret::RotateTurret(Vector2f spritePos, Vector2i mousePos)
{
	Vector2f diff;
	diff.x = mousePos.x - (width / 2);
	diff.y = mousePos.y - (height / 2);
	float angle = mouseAim->to_angle(mouseAim->normalized(diff));
	mSprite.setRotation(angle);
}

sf::Sprite hovercraftturret::GetSprite()
{
	return mSprite;
}

void hovercraftturret::Draw(sf::RenderWindow& App)
{
	for (int x = 0; x < playerprojV.size(); x++)
	{
		App.draw(playerprojV[x]->GetSprite());
	}
	App.draw(mSprite);
}

float hovercraftturret::GetX()
{
	return position.x;
}

float hovercraftturret::GetY()
{
	return position.y;
}

bool hovercraftturret::IsVisible()
{
	return true;
}

